(function($) {
  "use strict"; // Start of use strict

  const WESLEY_ID = 'wip34'
  const JOSEPH_ID = 'Joseph Distler'
  const JONATHAN_ID = 'Jonathan Collins'
  const IVAN_ID = 'IvanRadakovic'
  const SKYLAR_ID = 'Skylar'
  const COMMIT = 'commits'
  const ISSUE = 'issues'

  $('document').ready(function (){
    var text = '{\"'+JONATHAN_ID+'\":{"commits":0,"issues":0}, \
                 \"'+JOSEPH_ID+'\":{"commits":0,"issues":0}, \
                 \"'+WESLEY_ID+'\":{"commits":0,"issues":0}, \
                 \"'+SKYLAR_ID+'\":{"commits":0,"issues":0}, \
                 \"'+IVAN_ID+'\":{"commits":0,"issues":0}}'
    let userMap = JSON.parse(text)

    var total_commits = 0;
    var total_issues = 0;
    var jqxhr = $.get( "https://gitlab.com/api/v4/projects/8623060/repository/commits", function(data) {
      $.each(data, function(index){
        var author = data[index]['author_name'];
        if(author in userMap){
          userMap[author][COMMIT] = userMap[author][COMMIT] + 1;
        }
        total_commits++;
      });
    },'json')
    .done(function() {
    })
    .fail(function() {
    })
    .always(function() {
      $('#'+JONATHAN_ID.replace(' ','_')+'_commits').text('Commits: ' + userMap[JONATHAN_ID][COMMIT]);
      $('#'+WESLEY_ID.replace(' ','_')+'_commits').text('Commits: '+ userMap[WESLEY_ID][COMMIT]);
      $('#'+JOSEPH_ID.replace(' ','_')+'_commits').text('Commits: '+ userMap[JOSEPH_ID][COMMIT]);
      $('#'+IVAN_ID.replace(' ','_')+'_commits').text('Commits: '+ userMap[IVAN_ID][COMMIT]);
      $('#'+SKYLAR_ID.replace(' ','_')+'_commits').text('Commits: '+ userMap[SKYLAR_ID][COMMIT]);
      $('#total-commits').text('Total Commits: ' + total_commits);
    });

    var jqxhr = $.get( "https://gitlab.com/api/v4/projects/8623060/issues", function(data) {
      $.each(data, function(index){
        var author = data[index]['author']['name'];
        if(author in userMap){
          userMap[author][ISSUE] = userMap[author][ISSUE] + 1;
        }
        total_issues++;
      });
    },'json')
    .done(function() {
    })
    .fail(function() {
    })
    .always(function() {
      $('#'+JONATHAN_ID.replace(' ','_')+'_issues').text('Issues: '+ userMap[JONATHAN_ID][ISSUE]);
      $('#'+WESLEY_ID.replace(' ','_')+'_issues').text('Issues: '+ userMap[WESLEY_ID][ISSUE]);
      $('#'+JOSEPH_ID.replace(' ','_')+'_issues').text('Issues: '+ userMap[JOSEPH_ID][ISSUE]);
      $('#'+IVAN_ID.replace(' ','_')+'_issues').text('Issues: '+ userMap[IVAN_ID][ISSUE]);
      $('#'+SKYLAR_ID.replace(' ','_')+'_issues').text('Issues: '+ userMap[SKYLAR_ID][ISSUE]);
      $('#total-issues').text('Total Issues: ' + total_issues);
    });

  });

})(jQuery); // End of use strict
