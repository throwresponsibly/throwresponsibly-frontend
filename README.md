Software Engineering

CS373: Fall 2018

Project: ThrowResponsibly

Name: Joseph Distler
EID: jcd3229
GitLab: jdistler
Estimated Time: 6
Actual Time: 7

Name: Jonathan Collins
EID: jrc5776
GitLab: JCollins0
Estimated Time: 4
Actual Time: 6

Name: Skylar DonLevy
EID: sed2476
GitLab: skylar-donlevy
Estimated Time: 10
Actual Time: 7

Name: Ivan IvanRadakovic
EID: ir5694
GitLab: IvanRadakovic
Estimated Time: 6
Actual Time: 5

Name: Wesley Ip
EID: whi68
GitLab: wip34
Estimated Time: 4
Actual Time: 6

https://gitlab.com/throwresponsibly/throwresponsibly-frontend/pipelines
http://throwresponsibly.me
GIT SHA: d68036567127a0e228b34b0e18abd5846b901afe




This repo contains code for building a simple static website served using an Nginx container inside Docker. The code for the site is contained in `index.html`, and the Nginx config is in `default.conf`. The Dockerfile contains commands to build a Docker Image.

To build a Docker image from the Dockerfile, run the following command from inside this directory

```sh
$ docker build -t <docker-hub-username>/throwresponsibly:1.0 .
```

To run the image in a Docker container, use the following command
```sh
$ docker run -itd --name mycontainer --publish 80:80 <docker-hub-username>/throwresponsibly:1.0
```

This will start serving the static site on port 8080. If you visit `http://localhost:80` in your browser, you should be able to see our static site!
