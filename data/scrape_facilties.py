import requests
from pprint import pprint
import json as JSON

URL = "https://recyclinglocator.earth911.com/recycling-locator/a0f1303a5d272ba2/postal/"

URL2 = "https://recyclinglocator.earth911.com/recycling-locator/a0f1303a5d272ba2/listings/"

URL3 = "https://recyclinglocator.earth911.com/recycling-locator/a0f1303a5d272ba2/details/"

RECYCLABLE_IDS =[93, 429, 455,62, 61, 64, 245,83, 483,615,85, 488,645,529,332,489,490,228,491,495,553,377,362,497,492,573]

def scrape1(zip:str):
    headers = {'postal_code' : zip}
    # pprint(headers)
    r = requests.get(URL, params=headers)
    if r.status_code == 200:
        json = r.json()
        result = json['result']
        lat = "{0:.4f}".format(result['latitude'])
        lng = "{0:.4f}".format(result['longitude'])
        # print(lat, lng)
        for rec_id in RECYCLABLE_IDS:
            headers2 = {'what':rec_id, 'lat':lat, 'lng':lng}
            r2 = requests.get(URL2, params=headers2)

            if r2.status_code == 200:
                json2 = r2.json()

                if 'features' in json2:
                    features = json2['features']

                    # print("DISPOSAL FACILITIES")
                    for element in features:
                        geometry = element['geometry']
                        properties = element['properties']
                        if 'location_id' in properties:
                            id = properties['location_id']
                            headers3 = {'type':'location','ids': id}
                            r3 = requests.get(URL3, params=headers3)
                            if r3.status_code == 200:
                                json3 = r3.json()
                                result = json3['result']
                                data = result[id]
                                materials = data['materials']
                                materials_filtered = []

                                for mat in materials:
                                    materials_filtered.append(mat['description'])

                                output = {
                                'name' : data['description'],
                                'address' : data['address'],
                                'url' : data['url'],
                                'materials' : materials_filtered
                                }

                                print(JSON.dumps(output))

            # pprint(json2)


if __name__ == '__main__':
    scrape1('78702')
