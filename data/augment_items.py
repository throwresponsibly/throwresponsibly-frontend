import urllib
import requests
import time
import json
from pprint import pprint
import sys

DICTIONARY_HEADER = {
    'app_key' : "4852d797da3d88a5c85f89b6b20637d4",
    "app_id":"d5cef64e"
}

EBAY_HEADER = {
    'Authorization' : "Bearer v^1.1#i^1#p^1#f^0#r^0#I^3#t^H4sIAAAAAAAAAOVXW2wUVRjuttvWWi5KoBhszDJgosDMnJnt7s6O3Y3LtkgR2i1bixYNmZ05ww6dnRnmnKXd6ENTI5EgKpIYg7WQEIyYGECtD4AkomiMmD6QeglIAIPlosEnNEYSz8wuZVsJ1yIk7stm/vOf/3zf919mDuipqJqzduHaPyZ6Kku39oCeUo+HqwZVFeVzJ5WVzigvAUUOnq09s3u8vWWn65GU0S1xKUSWaSDo687oBhJdY4TK2oZoSkhDoiFlIBKxLCZjSxaLPANEyzaxKZs65WtqiFBKXTgVVlU/hKFAUA4oxGpcitlmRqgwpwaCfErg1XBdSJJlso5QFjYZCEsGjlA84ASaAzTg23hO5IAYCDEcF+6gfO3QRpppEBcGUFEXrujutYuwXh2qhBC0MQlCRZtiC5ItsaaGxua2erYoVrSgQxJLOItGP8VNBfraJT0Lr34Mcr3FZFaWIUIUG82fMDqoGLsE5ibgu1KrUFAAr4BwUPYDP5DGRcoFpp2R8NVxOBZNoVXXVYQG1nDuWooSNVKroIwLT80kRFODz/lrzUq6pmrQjlCN82PPxBIJKrrIRNBKN2g0Tttmlw0RnVjaQANVUIWAHE7RITUoK/6QUDgoH60g85iT4qahaI5oyNds4vmQoIZjtQFF2hCnFqPFjqnYQVTs57+kIRA6nKTms5jFacPJK8wQIXzu47UzMLIbY1tLZTEciTB2wZUoQkmWpSnU2EW3Fgvl040iVBpjS2TZrq4upsvPmPZKlgeAY59esjgpp2GGVEh3xun1vL927Q205lKRIdmJNBHnLIKlm9QqAWCspKJ+DoTCdQXdR8OKjrX+y1DEmR3dEePVIYoMwzxUUzCsQE6AcDw6JFooUtbBAVNSjs5IdifEli7JkJZJnWUz0NYU0R9Qeb+gQloJhlW6jgxFOhVQgjSnQgggTKXksPB/apTrLfWkbFowYeqanBuXgh+/YreVhGTjXBLqOjFcb9VfkSRySN52ek6v3xBFJwYiQSRLY5zaZmQzw5oSGWqOaYWL+pZ4a+R9eFcllRDMM9WU/IuMcekyaI3MkHYyszZ5hzMtzlxvMzuhQboE26auQ7uduyUlxm+i36FpfkVWsq4RGVfcbcxucEzeZG1L+A6y9vZ6ll+BORfwCwGuLhgI3RK3uJvXttx/MLRuKLELTYShchs+QNjR16Foifvjej0DoNezm9yoAAse5maBmRVlT3nLJsxAGoaMJqkM0lYa5CvfhkwnzFmSZpdWeJbX7tqxougCtvU58MDIFayqjKsuuo+B2ssr5dzk6RM5gQOA5zkOBEIdYNblVS9X4536+OZzk5vva/379Ed0dErX6zPPB177DEwccfJ4yktIZZR8UF9pfkula78/cODgofjmQNv9e4JfrFq/8dNHQvO+XDVtY+tvpxZPr/qksjq+Zcvsg3vW7lbffaIn4T+yKLnr5LIT9wy/d3JwnrykZkNf5blXvqpZd+Fs9+DL7ND6DuvrZTW//9W4eeaFY/y6vVrzphJpaPXbw8c/fycyYUGf/FD94Y+727f/0nhiyqHBfdFTx44Ozu1/8fmdLDe59uyf07Y/+2HmbDt99DF+zjr9yQe37TsUj22Yuo0+wwSrz3TaZd9l+4f2xH8y7B/3Hq4MzZIe3fHm8eGGTb8eeWlKqL7/yL0X17RP+vmb1UPC8FsD6dOtfRfx+f3v70xU79/5Q2bCq7UDc1MvDHi909a8kcun7x9+3ArEGg8AAA=="
}

DICTIONARY_SEARCH_URL = "https://od-api.oxforddictionaries.com:443/api/v1/search/en?q="
DICTIONARY_ENTRY_URL = "https://od-api.oxforddictionaries.com:443/api/v1/entries/en/"

EBAY_URL = "https://api.ebay.com/buy/browse/v1/item_summary/search?q="

def augment_list_data():
    data = sys.stdin.readlines()
    json_data = json.loads(data[0])
    materials = json_data['materials']
    count = 0
    augmented_items = {}
    for material in materials:
        # this API is rate limited to 60 requests / minute
        # so wait one minute before proceeding
        if count >= 59:
            time.sleep(61)
            count = 0
        urlencoded_query = urllib.parse.quote(material)

        # Make calls to dictionary
        dict_search_result = requests.get(DICTIONARY_SEARCH_URL+urlencoded_query, headers=DICTIONARY_HEADER)
        count += 1
        if dict_search_result.status_code == 200:
            output = dict_search_result.json()
            result = output['results']
            if len(result) > 0:
                top_result = result[0]
                result_id = top_result['id']
                # Another call to the dictionary API
                dict_entry_result = requests.get(DICTIONARY_ENTRY_URL+result_id, headers=DICTIONARY_HEADER)
                count += 1
                if dict_entry_result.status_code == 200:
                    entry_output = dict_entry_result.json()
                    entry_result = entry_output['results']
                    if len(entry_result) > 0:
                        top_entry_result = entry_result[0]
                        if "lexicalEntries" in top_entry_result:
                            lexicalEntries = top_entry_result["lexicalEntries"]
                            if len(lexicalEntries) > 0:
                                if "entries" in lexicalEntries[0]:
                                    entries = lexicalEntries[0]["entries"]
                                    if len(entries) > 0:
                                        if "senses" in entries[0]:
                                            senses = entries[0]["senses"]
                                            if len(senses) > 0:
                                                if "definitions" in senses[0]:
                                                    definitions = senses[0]["definitions"]
                                                    if len(definitions) > 0:
                                                        definition = definitions[0]
                                                        if material not in augmented_items:
                                                            augmented_items[material] = {"definition": definition}
                                                        else:
                                                            item_object = augmented_items[material]
                                                            item_object["definition"] = definition
                                                            augmented_items[material] = item_object

        # Make calls to ebay
        ebay_search_result = requests.get(EBAY_URL+urlencoded_query+"&limit=5", headers=EBAY_HEADER)
        if ebay_search_result.status_code == 200:
            output = ebay_search_result.json()
            if 'itemSummaries' in output:
                item_summaries = output['itemSummaries']
                titles = []
                image_urls = []
                item_web_url = []
                for summary in item_summaries:
                    if "title" in summary:
                        titles.append(summary["title"])
                    if "image" in summary:
                        image = summary["image"]
                        if "imageUrl" in image:
                            image_urls.append(image["imageUrl"])
                    if "itemWebUrl" in summary:
                        item_web_url.append(summary["itemWebUrl"])

                if material not in augmented_items:
                    augmented_items[material] = {"listings": {"titles":titles, "image_urls":image_urls, "item_web_url":item_web_url}}
                else:
                    item_object = augmented_items[material]
                    item_object["listings"] = {"titles":titles, "image_urls":image_urls, "item_web_url":item_web_url}
                    augmented_items[material] = item_object

    print(json.dumps(augmented_items))

if __name__ == '__main__':
    augment_list_data()
