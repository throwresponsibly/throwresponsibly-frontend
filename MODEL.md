# Throw Responsibly Model
Our data model can be found below. This will guide the database schema and REST API designed.

`Disposal Facilities`
*  id
*  name
*  address
*  description
*  list of items you can recycle at this facility (SQL JOIN with Recyclable Items table)
*  list of Recycling Programs that recycles items that you cannot recycle at this facility (the motivation behind this is to show the user alternative recycling options for items not recyclable at the facility in question. SQL LEFT JOIN with Recycling Programs table WHERE Recyclable Item is null)

`Recyclable Items`
*  id
*  name
*  description
*  list of disposal facilities where you can recycle this item  (SQL JOIN with Disposal Facilities table)
*  list of recycling Programs to recycle this item (SQL JOIN with Recycling Programs table)
*  list of ebay links of peopling selling said items (since you need a replacement for the one you're about to recycle).

`Recycling Programs`
*  id
*  name
*  description
*  list of items you can recycle with this program (SQL JOIN with Recyclable Items table)
*  list of Disposal Facilities that recycles items that you cannot recycle with this program (the motivation behind this is to show the user alternative recycling options for items not recyclable with the program in question. SQL LEFT JOIN with Disposal Facilities table WHERE Recyclable Item is null)  
